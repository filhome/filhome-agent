import { POPUP_TYPE } from '@/utils'
export const state = () => ({
  popup: {
    title: '',
    content: '',
    type: '',
    isShow: false,
    autoClose: false,
  },
})
export const getters = {
  popup(state) {
    return state.popup
  },
  isReset(state) {
    return state.popup.isReset
  },
  isShowPopup(state) {
    return state.popup.isShow
  },
  isAutoClose(state) {
    return state.popup.autoClose
  },
}
export const mutations = {
  SHOW_POPUP(state, payload) {
    state.popup.title = payload.title
    state.popup.content = payload.content
    state.popup.type = payload.type
    state.popup.autoClose = payload.autoClose
    state.popup.isShow = true
  },
  RESET_POPUP(state) {
    state.popup.isShow = false
    state.popup.title = ''
    state.popup.content = ''
    state.popup.type = POPUP_TYPE.ALERT
    state.popup.autoClose = false
  },
}
export const actions = {
  showPopup({ commit }, payload) {
    commit('SHOW_POPUP', payload)
  },
  resetPopup({ commit }) {
    commit('RESET_POPUP')
  },
}
