import { NOTIFIER_COLOR } from '@/utils'
export const state = () => ({
  notifier: {
    content: '',
    color: '',
    isReset: false,
  },
})
export const getters = {
  notifier(state) {
    return state.notifier
  },
  isReset(state) {
    return state.notifier.isReset
  },
}
export const mutations = {
  SHOW_MESSAGE(state, payload) {
    state.notifier.content = payload.content
    state.notifier.color = payload.color
    state.notifier.isReset = false
  },
  RESET_MESSAGE(state) {
    state.notifier.content = ''
    state.notifier.color = NOTIFIER_COLOR.INFO
    state.notifier.isReset = true
  },
}
export const actions = {
  showMessage({ commit }, payload) {
    commit('SHOW_MESSAGE', payload)
  },
  resetNotifier({ commit }) {
    commit('RESET_MESSAGE')
  },
}
