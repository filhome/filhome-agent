export const NOTIFIER_COLOR = {
  SUCCESS: 'PRIMARY',
  ERROR: 'ERROR',
  WARNING: 'WARNING',
  INFO: 'INFO',
}

export const POPUP_TYPE = {
  ALERT: 'ALERT',
  CONFIRM: 'CONFIRM',
}

export function listToTree(list) {
  try {
    if (list?.length === 0) return
    const map = {}
    let node
    const roots = []
    let i

    for (i = 0; i < list.length; i += 1) {
      map[list[i].id] = i // initialize the map
      list[i].children = [] // initialize the children
    }

    for (i = 0; i < list.length; i += 1) {
      node = list[i]
      if (node?.parentId?.id) {
        // if you have dangling branches check that map[node.parentId] exists
        list[map[node?.parentId?.id]].children.push(node)
      } else {
        roots.push(node)
      }
    }
    return roots
  } catch (error) {
    return []
  }
}
