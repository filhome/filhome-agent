export const rules = {
  required: (data) => !!data || 'Field required',
  email: (data) =>
    /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
      data
    ) || 'Email must be valid',
  min8Max40: (data) =>
    (data.length >= 8 && data.length <= 40) || 'Required 8 to 40 characters',
  specialCharacter: (data) =>
    /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/.test(
      data
    ) || 'Field must at least one letter, one number and one special character',
}
