import colors from 'vuetify/es5/util/colors'

export default {
  head: {
    titleTemplate: '%s - fil.agent',
    title: 'fil.agent',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  css: ['@/assets/scss/main.scss'],

  plugins: [
    '~plugins/notifier.js',
    '~plugins/popup.js',
    '~/plugins/localstorage.client.js',
  ],

  components: true,

  buildModules: [
    '@nuxtjs/eslint-module',
    '@nuxtjs/stylelint-module',
    '@nuxtjs/vuetify',
  ],

  modules: ['@nuxtjs/axios', '@nuxtjs/pwa', 'nuxt-i18n', '@nuxtjs/auth-next'],
  env: {
    strapiApi: process.env.STRAPI_API || 'http://localhost:1337/',
    siteHost: process.env.SITE_HOST || 'http://localhost:9323',
  },
  axios: {
    baseURL: process.env.STRAPI_API || 'http://localhost:1337/',
  },

  publicRuntimeConfig: {
    axios: {
      browserBaseURL: process.env.STRAPI_API || 'http://localhost:1337/',
    },
    siteHost: process.env.SITE_HOST || 'http://localhost:9323',
    strapiApi: process.env.STRAPI_API || 'http://localhost:1337/',
  },

  privateRuntimeConfig: {
    axios: {
      baseURL: process.env.STRAPI_API || 'http://localhost:1337/',
    },
    siteHost: process.env.SITE_HOST || 'http://localhost:9323',
  },
  auth: {
    strategies: {
      local: {
        token: {
          property: 'jwt',
        },
        user: {
          property: false,
          // autoFetch: true
        },
        endpoints: {
          login: {
            url: 'auth/local',
            method: 'post',
            propertyName: 'jwt',
          },
          user: { url: 'users/me', method: 'get' },
          logout: false,
        },
      },
      google: {
        responseType: 'code',
        codeChallengeMethod: '',
        grantType: 'authorization_code',
        clientId: process.env.GOOGLE_CLIENT_ID,
      },
    },
    redirect: {
      login: '/auth/signin',
      callback: '/auth/signin',
      home: '/',
    },
  },

  i18n: {
    langDir: '~/locales/',
    defaultLocale: 'en',
    strategy: 'no_prefix',
    locales: [
      { name: 'Tiếng Việt', code: 'vi', iso: 'vi-VN', file: 'vi.js' },
      { name: 'Korean', code: 'ko', iso: 'ko-KR', file: 'ko.js' },
      { name: 'English', code: 'en', iso: 'en-US', file: 'en.js' },
    ],
  },

  pwa: {
    manifest: {
      lang: 'en',
    },
  },

  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    treeShake: true,
    theme: {
      options: {
        customProperties: true,
      },
      dark: false,
      themes: {
        dark: {
          primary: '#1EB980',
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
          background: '#F6F7FB',
          surface: '#373740',
          onbackground: '#FFFFFF',
          onsurface: '#FFFFFF',
        },
        light: {
          primary: '#1EB980',
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
          background: '#F6F7FB',
          surface: '#FFFFFF',
          onbackground: '#fafafa',
          onsurface: '#373740',
        },
      },
    },
  },
  router: {
    middleware: ['auth'],
  },
  build: {},
}
