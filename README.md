# FILHOME AGENT

## Coding convention

```bash
# Filename
$ kebab-case
$ Ex: the-header.vue

# Component tag name
$ kebab-case
$ Ex: <the-header></the-header>

# Javascript namming
$ variable, method, property: camelCase
$ constant: UPPERCASE

# Css naming
$ BEM
```
## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```
## Nuxt auth

```bash
# Google auth
$ Using route.fullPath create get request to Strapi provider

# Local auth
$ Research auth module sourcecode check condition

# Facebook auth
$ .....
```