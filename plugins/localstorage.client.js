import createPersistedState from 'vuex-persistedstate'

export default ({ store }) => {
  createPersistedState({
    key: 'filhome.agent.com.vn',
    paths: ['property'],
  })(store)
}
