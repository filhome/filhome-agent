import { POPUP_TYPE } from '@/utils'
export default ({ app, store }, inject) => {
  inject('popup', {
    show({
      title = '',
      content = '',
      autoClose = false,
      type = POPUP_TYPE.ALERT,
    }) {
      store.dispatch('popup/showPopup', {
        title,
        content,
        autoClose,
        type,
      })
    },

    alert({ title = '', content = '', autoClose = false }) {
      store.dispatch('popup/showPopup', {
        title,
        content,
        autoClose,
        type: POPUP_TYPE.ALERT,
      })
    },

    confirm({ title = '', content = '', autoClose = false }) {
      store.dispatch('popup/showPopup', {
        title,
        content,
        autoClose,
        type: POPUP_TYPE.CONFIRM,
      })
    },
  })
}
