import { NOTIFIER_COLOR } from '@/utils'
export default ({ app, store }, inject) => {
  inject('notifier', {
    show({ content = '', color = '' }) {
      store.dispatch('notifier/showMessage', {
        content,
        color,
      })
    },

    success(content = '') {
      store.dispatch('notifier/showMessage', {
        content,
        color: NOTIFIER_COLOR.SUCCESS,
      })
    },

    error(content = '') {
      store.dispatch('notifier/showMessage', {
        content,
        color: NOTIFIER_COLOR.ERROR,
      })
    },

    warning(content = '') {
      store.dispatch('notifier/showMessage', {
        content,
        color: NOTIFIER_COLOR.WARNING,
      })
    },

    info(content = '') {
      store.dispatch('notifier/showMessage', {
        content,
        color: NOTIFIER_COLOR.INFO,
      })
    },
  })
}
